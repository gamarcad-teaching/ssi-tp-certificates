#! /bin/bash


# checks that openssl is installed and accessible
if [ ! $(which openssl) ]; then
    echo "Openssl is not installed or not accessible."
    echo "Please, install openssl using"
    echo "  apt install openssl" 
    exit 1
fi

# checks that python3 is installed and accessible
if [ ! $(which python3) ]; then
    echo "Python 3.X is not installed or not accessible."
    echo "Please install Python 3 on your system."
    exit 1
fi  

echo "All tests passed"