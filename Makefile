certificates:
	./certificates.sh

lesson:
	asciidoctor-pdf -D lesson README.adoc

server:
	python3 run-server.py

.PHONY: correction lesson server