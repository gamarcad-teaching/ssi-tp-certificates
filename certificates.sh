# Deletion of previous CAs working folders
if [ -d root_ca ]; then rm -Rf root_ca; fi
if [ -d dsi_ca ]; then rm -Rf dsi_ca; fi

# creation of Admin CA working folder
mkdir root_ca
touch root_ca/index
echo "00" > root_ca/serial

# creation of DSI CA working folder
mkdir dsi_ca
touch dsi_ca/index
echo "00" > dsi_ca/serial

# certificate generation for Admin CA
openssl genrsa -out root.key 2048
openssl req -new -key root.key -out root.csr -config root_req.config
openssl ca -batch -in root.csr -out root.pem -config root.config -selfsign -extfile ca.ext -days 1095

# certificate generation for DSI CA
openssl genrsa -out dsi.key 2048
openssl req -new -key dsi.key -out dsi.csr -config dsi_req.config
openssl ca -batch -in dsi.csr -out dsi.pem -config root.config -extfile ca.ext -days 730

# certificate generation for the web server
openssl genrsa -out server.key 2048
openssl req -new -key server.key -out server.csr -config server_req.config
openssl ca -batch -in server.csr -out server.pem -config dsi.config -days 365 -extfile server.ext

# ensure that it is possible to verify the web server certificate
openssl verify -x509_strict -CAfile root.pem -untrusted dsi.pem server.pem