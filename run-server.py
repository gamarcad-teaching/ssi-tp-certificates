from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl

# Object used to request handler.
class HelloWorldHTTPRequestHandler(BaseHTTPRequestHandler):
        def do_GET(self):
                self.send_response(200)
                self.end_headers()
                self.wfile.write("<h1>Hello World !</h1>".encode())


# The HTTP server
httpd = HTTPServer(('127.0.0.1', 4443), HelloWorldHTTPRequestHandler)
httpd.socket = ssl.wrap_socket (
        httpd.socket, 
        keyfile="server.key", 
        certfile='server.pem', 
        server_side=True
        )

# Launch the server
httpd.serve_forever()